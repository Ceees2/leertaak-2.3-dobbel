import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
    
public class ThrowsView extends JPanel implements ActionListener
{
    private DobbelsteenModel d;
    private HashMap<Integer, Integer> throwsCount = new HashMap<Integer, Integer>();
    //private JPanel textPanel = new JPanel(new FlowLayout());
    int totalThrows = 0;
    
    private JTextField totalThrowsTextField = new JTextField();
    private JTextField throwOneTextField = new JTextField();
    private JTextField throwTwoTextField = new JTextField();
    private JTextField throwThreeTextField = new JTextField();
    private JTextField throwFourTextField = new JTextField();
    private JTextField throwFiveTextField = new JTextField();
    private JTextField throwSixTextField = new JTextField();
    
    private JLabel labelTotalThrows = new JLabel("worpen");
    private JLabel labelOne = new JLabel("1: ");
    private JLabel labelTwo = new JLabel("2: ");
    private JLabel labelThree = new JLabel("3: ");
    private JLabel labelFour = new JLabel("4: ");
    private JLabel labelFive = new JLabel("5: ");
    private JLabel labelSix = new JLabel("6: ");
    
	public ThrowsView()
	{
	    this.setLayout(new GridLayout(7,2));
	    this.add(totalThrowsTextField);
	    this.add(labelTotalThrows);
	    this.add(labelOne);
	    this.add(throwOneTextField);
	    this.add(labelTwo);
	    this.add(throwTwoTextField);
	    this.add(labelThree);
	    this.add(throwThreeTextField);
	    this.add(labelFour);
	    this.add(throwFourTextField);
	    this.add(labelFive);
	    this.add(throwFiveTextField);
	    this.add(labelSix);
	    this.add(throwSixTextField);
	    throwsCount.put(1, 0);
	    throwsCount.put(2, 0);
	    throwsCount.put(3, 0);
	    throwsCount.put(4, 0);
	    throwsCount.put(5, 0);
	    throwsCount.put(6, 0);
	}
	
	public void actionPerformed( ActionEvent e )
	{
	    totalThrows ++;
	    d = (DobbelsteenModel) e.getSource();
		throwsCount.put(d.getWaarde(), throwsCount.get(d.getWaarde()) + 1);
	    totalThrowsTextField.setText(""+totalThrows);
	    throwOneTextField.setText(""+throwsCount.get(1));
	    throwTwoTextField.setText(""+throwsCount.get(2));
	    throwThreeTextField.setText(""+throwsCount.get(3));
	    throwFourTextField.setText(""+throwsCount.get(4));
	    throwFiveTextField.setText(""+throwsCount.get(5));
	    throwSixTextField.setText(""+throwsCount.get(6));
	}
	
	public Dimension getPreferredSize()
	{
	    return new Dimension(100,50);
	} 
}